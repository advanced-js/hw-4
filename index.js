"use strict";

/*Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript. 

AJAX - це спеціальний термін, який позначає технологію для асинхронних запитів на сервер. Завдяки AJAX можна підвантажувати дані з серверу без перезавантаження сторінки. Це дозволяє оновлювати вміст сторінки поступово, залежно від потреб користувача та логіки програми. Використання AJAX робить взаємодію користувача та програми більш зручною та динамічною.  

*/ 

const URL = "https://ajax.test-danit.com/api/swapi/films";


class Request {
    getDataServer(url){
            return fetch(url)
            .then(response => {
                if(response.ok){
                    return response.json()
                } else {
                    return response.json().then(error => {
                                        const e = new Error('Что-то пошло не так');
                                        e.data = error;
                                        throw e;
                                    })
                }
            })
            }
}


class Films {

    renderFilms(){
        const request = new Request();
        return request.getDataServer(URL)
        .then((data) => {
            try {
                data.map(({name, episodeId, openingCrawl, id}) => {
                        let li = document.createElement("li");
                        li.textContent = `Епізод - ${episodeId}: ${name.toUpperCase()} - ${openingCrawl}`;
                        ul.append(li);
                        let ul2 = document.createElement("ul");
                        ul2.id = id;
                        li.append(ul2);
        }
        )
        return data;
            } catch (err) {
               console.log(err);
            }
            
        })
            }

    renderCharacters(data){
                    data.map(({id, characters}) => {
                     let res =  characters.map(link => 
                            fetch(link));
                        
                        Promise.all(res)
                        .then(responses => Promise.all(responses.map(r => r.json())))
                        .then(data => {
                                data.map(({name}) => {
                                    let character = document.createElement("li");
                                    character.textContent = name;
                                    let elem = document.getElementById(id);
                                    elem.append(character)
                                            })
                                })
                                    
                         });
                }

}
let ul = document.createElement("ul");
document.body.prepend(ul);


let films = new Films();
films.renderFilms()
.then(data => {films.renderCharacters(data)});



